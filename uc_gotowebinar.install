<?php

/**
 * @file Install functions.
 */

/**
 * Implementation of hook_install().
 */
function uc_gotowebinar_install() {
  // Create an Ubercart product class and node type.
  _uc_gotowebinar_create_uc_product_type('gotowebinar', 'GoToWebinar', 'Sell a GoToWebinar event registration.');
}

/**
 * Implementation of hook_uninstall().
 */
function uc_gotowebinar_uninstall() {
  // Remove all module variables from the database
  db_query("DELETE FROM {variable} WHERE name LIKE 'uc_gotowebinar_%%'");
  
  // Delete "gotowebinar" nodes.
  $type = 'gotowebinar';
  $result = db_query("SELECT nid FROM {node} WHERE type = '%s'", $type);
  while ($nid = db_result($result)) {
    node_delete($nid);
  }

  // Delete the product_class and the node_type.
  db_query("DELETE FROM {uc_product_classes} WHERE pcid = '%s'", $type);
  module_invoke_all('product_class', $type, 'delete');
  node_type_delete($type);

  node_types_rebuild();
  menu_rebuild();
}

/**
 * Create a new product class.
 * Idea taken from http://stackoverflow.com/a/4732132/955858
 *   and ubercart/uc_product/uc_product.admin.inc::uc_product_class_form_submit()
 */
function _uc_gotowebinar_create_uc_product_type($pcid, $name, $description) {
  // Convert whitespace to underscores, and remove other non-alphanumeric characters.
  $pcid = preg_replace(array('/\s+/', '/\W/'), array('_', ''), strtolower($pcid));

  db_query("INSERT INTO {uc_product_classes} (pcid, name, description) VALUES ('%s', '%s', '%s')", $pcid, $name, $description);
  uc_product_node_info(TRUE);
  variable_set('node_options_'. $pcid, variable_get('node_options_product', array('status', 'promote')));

  if (module_exists('comment')) {
    variable_set('comment_'. $pcid, variable_get('comment_product', COMMENT_NODE_READ_WRITE));
  }

  // Invoke the insertion of the class and node type.
  module_invoke_all('product_class', $pcid, 'insert');
  node_types_rebuild();

  // Add node type settings and associated fields.
  variable_set('uc_product_shippable_'. $pcid, 0);
  _uc_gotowebinar_create_node_type_fields($pcid);
  if (module_exists('imagefield')) {
    uc_product_add_default_image_field($pcid);
  }
  menu_rebuild();

  drupal_set_message(t('Product class !name (!class) created.', array('!name' => $name, '!class' => $pcid)));
}

/**
 * Create fields on the content type.
 */
function _uc_gotowebinar_create_node_type_fields($type) {
  module_load_include('inc', 'content', 'includes/content.crud');

  $t = get_t();

  $label = $t('GoToWebinar Registration URL');
  $field_name = 'field_gotowebinar_url';
  $field = array(
    'label' => $label,
    'field_name' => $field_name,
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'weight' => '-4',
    'rows' => 5,
    'size' => '128',
    'description' => $t('The registration URL of a GoToWebinar event in the format <em>@url</em>.', array('@url' => 'https://attendee.gotowebinar.com/register/#######')),
    'default_value_widget' => NULL,
    'group' => false,
    'required' => 1,
    'multiple' => '0',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'module' => 'text',
    'widget_module' => 'text',
    'columns' => array(
      'value' => array(
        'type' => 'text',
        'size' => 'big',
        'not null' => false,
        'sortable' => true,
        'views' => true,
      ),
    ),
    'display_settings' => array(
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'hidden',
      ),
      'full' => array(
        'format' => 'hidden',
      ),
      4 => array(
        'format' => 'hidden',
      ),
    ),
  );

  // Only add the field if it doesn't exist. Don't overwrite any changes.
  $instances = content_field_instance_read(array('field_name' => $field_name, 'type_name' => $type));
  if (count($instances) < 1) {
    $prior_instances = content_field_instance_read(array('field_name' => $field_name));
    if ($prior_instances) {
      // Copy the prior instance.
      content_field_instance_create(array('field_name' => $field_name, 'type_name' => $type));
    }
    else {
      // Create a new field and instance.
      $field['field_name'] = $field_name;
      $field['type_name'] = $type;
      content_field_instance_create($field);
    }
  }
}