This module creates a GoToWebinar product and provides a few basic
automated tasks via the GoToWebinar API.

GoToWebinar product type nodes follow the standard cart/checkout
process in Ubercart. Once the Order status (in Ubercart) is set to
"Completed", customers are automatically added to the webinar event.

Each webinar event has a "GTW Registrants" tab which shows a quick view
of all registrants.

Installation and Usage
======================

You must configure the API credentials which are found under Store
administration > Configuration > GoToWebinar settings.

This module adds a GoToWebinar product class and content type, and it
contains a custom text field called "GoToWebinar URL"
(field_gotowebinar_url). When creating new webinar nodes, copy the
registration URL from the GoToWebinar dashboard page and add it to that
field on the node.
