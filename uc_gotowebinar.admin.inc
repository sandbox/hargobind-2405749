<?php

/**
 * @file
 * Administration pages.
 */

/**
 * Admin settings page.
 */
function uc_gotowebinar_admin_settings() {
  $form['gotowebinar_api'] = array(
    '#type' => 'fieldset',
    '#title' => 'API credentials',
  );
  
  $form['gotowebinar_api']['instructions'] = array(
    '#type' => 'markup',
    '#value' => '
      <p>These steps describe how to create the necessary GoTo/Citrix accounts and set up API credentials via the <a href="https://developer.citrixonline.com/page/direct-login" target="_blank">Direct Login</a> method.</p>
      <ol>
        <li>Create a <a href="http://www.gotomeeting.com/online/webinar" target="_blank">GoToWebinar account</a> if you don\'t already have one.</li>
        <li>Visit the <a href="https://developer.citrixonline.com/user/me/apps" target="_blank">Citrix Developer Center</a> to create an API account. This account is different than your GoToWebinar account.</li>
        <li>Once your Developer account is created, go to your profile and click the "<a href="https://developer.citrixonline.com/user/me/apps" target="_blank">My Apps</a>" link.</li>
        <li>Create a new application. Enter the custom name and description, and choose <b>GoToWebinar</b> for the <em>Product API</em>. Since we are using the <em>"Direct Login"</em> authentication method, enter <b>https://api.citrixonline.com</b> into the <em>Application URL</em> field.</li>
        <li>Once your application is created, copy the <em>"Consumer Key"</em> and paste it into the <em>"App key"</em> field below.</li>
        <li>Enter your GoToWebinar account email and password and press <em>Save</em>. The tokens in the <em>Manual configuration</em> section below will be automatically retrieved.</li>
      </ol>
    ',
  );

  $form['gotowebinar_api']['uc_gotowebinar_app_key'] = array(
    '#type' => 'textfield',
    '#title' => t('App key'),
    '#default_value' => variable_get('uc_gotowebinar_app_key', ''),
    '#description' => t('This is the App key (also known as the Consumer Key) that identifies the specific application you created in the <a href="https://developer.citrixonline.com/user/me/apps" target="_blank">Citrix Developer Center</a>'),
  );

  $form['gotowebinar_api']['uc_gotowebinar_product_email'] = array(
    '#type' => 'textfield',
    '#title' => t('GoToWebinar email'),
    '#default_value' => variable_get('uc_gotowebinar_product_email', ''),
  );

  $form['gotowebinar_api']['uc_gotowebinar_product_password'] = array(
    '#type' => 'password',
    '#title' => t('GoToWebinar password'),
  );

  $form['gotowebinar_api']['manual'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manual configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['gotowebinar_api']['manual']['intro'] = array(
    '#type' => 'markup',
    '#value' => '<p>If the automated feature above is not working, or you want to manually generate your OAuth Access Token, visit the <a href="https://developer.citrixonline.com/page/authentication-and-authorization" target="_blank">Authentication and Authorization</a> API page for detailed instructions.</p>',
  );

  $form['gotowebinar_api']['manual']['uc_gotowebinar_organizer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Organizer key'),
    '#default_value' => variable_get('uc_gotowebinar_organizer_key', ''),
  );

  $form['gotowebinar_api']['manual']['uc_gotowebinar_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Access token'),
    '#default_value' => variable_get('uc_gotowebinar_access_token', ''),
  );

  $form['gotowebinar_cart'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shopping cart behavior'),
    '#collapsible' => TRUE,
  );
  $form['gotowebinar_cart']['uc_gotowebinar_cart_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Button type for adding to cart'),
    '#description' => t('The <em>Add to cart</em> button will take the user to their shopping cart. The <em>Buy now</em> button will take the user to the checkout page.'),
    '#options' => array(
      'add_to_cart' => t('Add to cart'),
      'buy_it_now' => t('Buy now'),
      'all' => t('Both'),
    ),
    '#default_value' => variable_get('uc_gotowebinar_cart_button_type', 'add_to_cart'),
  );
  $form['gotowebinar_cart']['buy_it_now'] = array(
    '#type' => 'fieldset',
    '#title' => t('Buy now button'),
    '#description' => t('The options below only apply when the <em>Buy now</em> button is visible.'),
  );
  $form['gotowebinar_cart']['buy_it_now']['uc_gotowebinar_cart_button_buy_it_now_show_in_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the <em>Buy now</em> button in product node teasers.'),
    '#default_value' => variable_get('uc_gotowebinar_cart_button_buy_it_now_show_in_teaser', FALSE),
  );
  $form['gotowebinar_cart']['buy_it_now']['uc_gotowebinar_cart_button_buy_it_now_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Title for <em>Buy now</em> button'),
    '#description' => t('Enter the text that is displayed on the <em>Buy now</em> button.'),
    '#default_value' => variable_get('uc_gotowebinar_cart_button_buy_it_now_text', t('Buy now')),
  );
  $form['gotowebinar_cart']['buy_it_now']['uc_gotowebinar_cart_button_buy_it_now_weight'] = array(
    '#type' => 'radios',
    '#title' => t('Button order'),
    '#description' => t('If both buttons are displayed, select which button is displayed first.'),
    '#options' => array(
      '1' => t('<em>Add to cart</em> first'),
      '0' => t('<em>Buy now</em> first'),
    ),
    '#default_value' => variable_get('uc_gotowebinar_cart_button_buy_it_now_weight', 1),
  );

  $node_types = node_get_types('names');
  $form['node_types_container'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['node_types_container']['node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('GoToWebinar content types'),
    '#description' => t('Choose the types on which you would like to associate GoToWebinar registrations. Each type must have a field named "@field".',
      array('@field' => 'field_gotowebinar_key')),
    '#options' => $node_types,
    '#default_value' => variable_get('uc_gotowebinar_node_types', array('gotowebinar')),
  );

  $form['#submit'][] = 'uc_gotowebinar_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Communicate with the Citrix GoTo API to use the Direct Login method to retrieve OAuth tokens.
 * @see https://developer.citrixonline.com/page/direct-login
 */
function uc_gotowebinar_admin_settings_submit(&$form, &$form_state) {
  // Change the name of the node type variable and clean it up.
  $node_types = array_keys(array_filter($form_state['values']['node_types']));
  $form_state['values']['uc_gotowebinar_node_types'] = $node_types;
  unset($form_state['values']['node_types']);

  // Parameters for the GoTo API.
  $params = array(
    'grant_type' => 'password',
    'user_id' => $form_state['values']['uc_gotowebinar_product_email'],
    'password' => $form_state['values']['uc_gotowebinar_product_password'],
    'client_id' => $form_state['values']['uc_gotowebinar_app_key'],
  );
  $gtw_url = "https://api.citrixonline.com/oauth/access_token";
  
  if (empty($params['user_id']) || empty($params['client_id'])) {
    return;
  }
  else if (empty($params['password'])) {
    drupal_set_message(t('You must re-enter your password if you want to regenerate the API tokens.'), 'warning');
    return;
  }

  // GoToWebinar submission
  $gtw = _gtw_api_request($gtw_url, $params, array('auth' => FALSE));

  if (!($gtw->response['http_code'] == 200 || $gtw->response['http_code'] == 201)) {
    $err = $gtw->results;
    if (isset($gtw->results->int_err_code)) {
      $err->error_type = $err->int_err_code;
    }
    else {
      $err->msg = $err->error;
      if ($gtw->response['http_code'] == 401)
        $err->error_type = t('Invalid API key');
      else
        $err->error_type == t('Invalid call');
    }
    drupal_set_message(t('There was an error validating your credentials.<br>Error: <em>!err</em> (!code)<br>Details: <em>!msg</em>',
    array('!err' => $err->error_type, '!code' => $gtw->response['http_code'], '!msg' => $err->msg)), 'error');
  }
  // Success. Save the values.
  else {
    $form_state['values']['uc_gotowebinar_access_token'] = $gtw->results->access_token;
    $form_state['values']['uc_gotowebinar_organizer_key'] = $gtw->results->organizer_key;
    $form_state['values']['uc_gotowebinar_product_password'] = '';
    drupal_set_message(t('GoToWebinar API tokens have been retrieved.'));
  }
}

/**
 * View registrants via the GoToWeb API.
 * https://developer.citrixonline.com/api/gotowebinar-rest-api/apimethod/get-registrants
 *
 * @param $node
 *   The webinar node.
 */
function uc_gotowebinar_view_registrants($form_state, $node) {
  $webinar_key = NULL;
  if (isset($node->field_gotowebinar_url)) {
    $webinar_key = $node->field_gotowebinar_url[0]['value'];
  }
  if (empty($webinar_key)) {
    drupal_set_message(t('This node is not a valid webinar (i.e. it is missing the webinar URL).'), 'error');
    return FALSE;
  }
  
  preg_match('#([0-9]+)$#', $webinar_key, $matches);
  $webinar_key = $matches[1];
  $organizer_key = variable_get('uc_gotowebinar_organizer_key', '');
  $gtw_url = "/organizers/$organizer_key/webinars/$webinar_key/registrants";

  // GoToWebinar submission
  $gtw = _gtw_api_request($gtw_url, $params);

  if (!($gtw->response['http_code'] == 200 || $gtw->response['http_code'] == 201)) {
    $webinar_link = l(t('view webinar'), 'node/' . $node->nid);
    watchdog("uc_gotowebinar", "Error getting registrants for the webinar.<br>Results: <pre>!results</pre>",
      array('!results' => check_markup(print_r($gtw->results, 1))), WATCHDOG_ERROR, $webinar_link);
    drupal_set_message(t('There was an error getting the webinar information.<br>Results: <pre>!results</pre>', array('!results' => check_plain(print_r($gtw->results, 1)))), 'error');
    return FALSE;
  }

  // Get the results.
  $header = array();
  $rows = array();
  foreach ($gtw->results as $i => $reg) {
    $rows[] = array(
      '#' => count($gtw->results) - $i,
      'First Name' => $reg->firstName,
      'Last Name' => $reg->lastName,
      'Email' => l($reg->email, 'mailto:' . $reg->email),
      'Status' => $reg->status,
      'Reg Date' => date('M j, Y h:i a', strtotime($reg->registrationDate)),
      'Join Url' => '<input type="text" value="' . $reg->joinUrl . '" onfocus="this.select()">',
    );
  }
    
  if (count($rows)) {
    $header = array_keys(current($rows));
    $build['registrants'] = array(
      '#type' => 'markup',
      '#value' => theme('table', $header, $rows),
    );
  }
  else {
    drupal_set_message(t('No registrants found.'), 'warning');
  }

  return $build;
}
